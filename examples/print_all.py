from pyQIS import HTWKLeipzigQIS

htwk = HTWKLeipzigQIS()

logged_in = htwk.login("username", "password")

if logged_in:
    print("Logged in successfully!\n")

    results = htwk.fetch_results()
    for semester in results:
        print("{}".format(semester))
        for grade in semester.grades:
            if semester.grade_average_exam_identifier in grade.exam_id: # Skip invalid 'Modulprüfungen'
                continue
            print("  {}".format(grade))
        (is_calculated, grade) = semester.grade_average()
        print("Grade average: {} (calculated: {})".format(grade, is_calculated))
        print("---")
    
    htwk.logout()
    print("Logged out!")
else:
    print("Login failed!")