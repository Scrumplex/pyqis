pyQIS
-----
QIS client for German university information servers.

# Compatibility
pyQIS is very much work in progress right now and only supports the following universities' QIS servers:
- HTWK Leipzig - https://qisserver.htwk-leipzig.de/qisserver

# Usage
An example use is provided in [examples/](examples)

# License
This project is licensed under the terms of the GNU General Public License 3.0. Read the full license text [here](LICENSE)
